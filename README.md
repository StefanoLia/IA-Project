# Project of Artificial Intelligence for the University of Padua

**Students**: Lia Stefano and Zanatta Alberto (MSc students in Computer Science)

## Description

This project is an implementation and an extension of the paper written by (Manish Jain, Vincent Conitzer, Milind Tambe) and the paper
written by (Manish Jain, Dmytro Korzhyk, Ondřej Vaněk+ Michal Pěchouček, Vincent Conitzer , Milind Tambe).

In the code folder you can find the implementation of:

* Rugged algorithm;
* Snares algorithm;

All the other files contain the heuristics needed to implement and to outline the algorithms. All the details are explained in the 
**paper folder**, which contains a file **paper.pdf** which explains more specifically the structure of the program and its purpose.

## System Requirements

In order to run the program you need:
* Python (v. 3.6.4);
* AMPL (v. 20180511);
* CPLEX (v. 12.8.0.0);

The maps which are shown as a graphical result are plotted using **osmnx** provided by Geoff Boeing.

However, we added more functionalities to the library. You can find the new version and the instruction to install it [here](https://github.com/StefanoLia/osmnx)