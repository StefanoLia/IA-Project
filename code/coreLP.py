from solver import *


def coreLP(attacks, allocations, targets, z):
    attacks_set = [i for i in range(len(attacks))]
    allocations_set = [i for i in range(len(allocations))]

    # print("z", z)
    # print("attack_set", attacks)
    # print("allocations_set", allocations)

    values = {
        (allocation, attack): z[i][j]
        for i, allocation in enumerate(allocations_set)
        for j, attack in enumerate(attacks_set)
        }

    mod_file_defender = "./models/coreLP_defender.mod"
    sets = {'Attacks': attacks_set, 'Allocations': allocations_set}
    parameters = {'t': (False, targets), 'z': (False, values)}

    solver_defender = Solver(mod_file_defender, sets, parameters)
    solver_defender.solve()

    mod_file_attacker = "./models/coreLP_attacker.mod"
    solver_attacker = Solver(mod_file_attacker, sets, parameters)
    solver_attacker.solve()

    mixed_defender_strategy = [x[1] for x in solver_defender.get_variable('x').getValues().toList()]
    mixed_attacker_strategy = [a[1] for a in solver_attacker.get_variable('a').getValues().toList()]
    # print("mixed_defender_strategy: ", mixed_defender_strategy)
    # print("mixed_attacker_strategy: ", mixed_attacker_strategy)
    defender_utility = solver_defender.get_variable('v').getValues().toList()[0]
    attacker_utility = solver_attacker.get_variable('v').getValues().toList()[0]
    # print("defender_utility: ", defender_utility)
    # print("attacker_utility: ", attacker_utility)
    return (mixed_defender_strategy, defender_utility), (mixed_attacker_strategy, attacker_utility)