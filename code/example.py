import networkx as nx
import numpy as np
from solver import *
from rugged import *
from snares import *

graph = nx.Graph()

graph.add_edge(1, 2, weight=1)
graph.add_edge(3, 1, weight=1)
graph.add_edge(1, 4, weight=1)
graph.add_edge(2, 3, weight=1)
graph.add_edge(4, 3, weight=1)

# print(rugged(graph, 1, 1, 3, 5))
print(snares(graph, 1, 1, 3, 5))
'''
nodes = list(G.nodes())
edges = list(G.edges())
attack_paths = [[(1, 2),(2, 3)],[(1, 3)]]
t = [2, 2]
a = [0.4, 0.6]
allocations = []
resources = 1
print("nodes: ", nodes)
print("edges: ", edges)

sets = {"Nodes":nodes, "Edges":edges, "Attacks": [i for i in range(len(attack_paths))]}
indexed_sets = {"AttackPaths":attack_paths}

parameters = {'k': (True, resources), 't': (False, t), 'a': (False, a)}

defender_oracle_mod = "./models/prova.mod"

solver = Solver(defender_oracle_mod, sets, parameters, indexed_sets)

solver.solve()



tuple_edges = G.edges()

key_edges = {}
key_attacker_paths = {}
k = 2
allocation = [[1, 2], [1, 3]]  # [arco con codice 1, arco con codice 2]
attack_path = [[0, 3], [1, 3]]

edges = np.arange(len(G.edges()))
paths = np.arange(len(attack_path))

# print(edges, paths)

for index, e in enumerate(tuple_edges):
    key_edges[index] = e

for index, path in enumerate(attack_path):
    key_attacker_paths[index] = path

print("key_edges:", key_edges)
print("key_attacker_paths:", key_attacker_paths)

a = [0.5, 0.5]
t = [2, 2]

A = np.zeros((len(attack_path), len(edges)))

for i in key_attacker_paths:
    for e in key_attacker_paths[i]:
        A[i, e] = 1

values = {
    (attack, edge): A[i][j]
    for i, attack in enumerate([1, 2])
    for j, edge in enumerate([0, 1, 2, 3])
}

sets = {'Attacks': [1, 2], 'Edges': [0, 1, 2, 3]}
parameters = {'k': (True, 2), 't': (False, t), 'a': (False, a), 'A': (False, values)}

defender_oracle_mod = "./models/defender_oracle.mod"

solver = Solver(defender_oracle_mod, sets, parameters)

solver.solve()
'''




