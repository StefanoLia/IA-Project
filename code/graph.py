import networkx as nx
import osmnx as ox
import matplotlib.pyplot as plt


class Graph:

    def __init__(self, city):
        self.ox_graph = ox.graph_from_address(city)
        self.G = nx.DiGraph()
        self.dict_nodes = {}
        self.dict_index = {}
        self.city = city

        for index, node in enumerate(self.ox_graph.nodes()):
            self.dict_nodes[node] = index
            self.dict_index[index] = node

        edges = []
        ox_edges = self.ox_graph.edges()
        print("OX NODES", list(self.ox_graph.nodes()))

        for edge in ox_edges:
            # edge = ox_nodes[i]
            edges.append((edge[0], edge[1]))

        for edge in edges:
            self.G.add_edge(self.dict_nodes[edge[0]], self.dict_nodes[edge[1]], weight=1.0)

        self.old_copy = self.G.copy()

    def get_graph(self):
        return self.G

    def print_graph(self):
        ox.plot_graph(self.ox_graph)

    def print_graph_with_routes(self, routes, targets):
        real_routes = []
        for route in routes:
            real_routes.append([self.dict_index[r] for r in route])

        # archi = self.ox_graph.edges()
        #
        # for route in real_routes:
        #     i=0
        #     while i < len(route)-1:
        #         if (route[i], route[i+1]) not in archi:
        #             print("NO", (route[i], route[i+1]))
        #         i=i+1
        plt.clf()
        fig, ax = ox.plot_graph_routes(self.ox_graph, real_routes, targets, show=False, close=False)
        ax.set_title("Path attacks on {}".format(self.city))
        plt.show()

    def save_graph_with_routes(self, routes, targets, chi):
        real_routes = []
        for route in routes:
            real_routes.append([self.dict_index[r] for r in route])
        plt.clf()
        fig, ax = ox.plot_graph_routes(self.ox_graph, real_routes, targets, show=False, close=False)
        ax.set_title("Path attacks on {}".format(self.city))
        plt.savefig("images/path_folder/"+self.city+"_graph_with_routes_{}.png".format(chi))

    def get_real_nodes(self):
        return self.ox_graph.nodes()

    def get_cripted_node(self, node):
        return self.dict_nodes[node]

    def refactor(self):
        self.G = self.old_copy.copy()