from utils.minheap import MinHeap


def abo(X, x, graph, source, targets):
    """
    This function calculates the better strategy of the attacker.
    :param X: allocations of the defender
    :param x: defender mixed strategy (in probabilities)
    :param graph: the map of the urban area
    :param source: source node
    :param targets: targets node
    :return: tuple: greedy strategy of the attacker and associated utility
    """

    attacker_response = []
    node_list = list(graph.nodes())
    caught = {}
    pred = {}
    allocations = {}

    for n in node_list:
        caught[n] = float('inf')
        pred[n] = None
    caught[source] = 0

    allocations[source] = X[:]

    Q = MinHeap()
    Q.add((source, caught[source]))

    while not Q.empty:
        u = Q.pop()  # extract the min node (which has the min probability to be caught)
        for v in graph.neighbors(u):
            edge = (u, v)
            weight, crossed_allocs = cost(edge, allocations[u], x)  # calculate the weight associated to this edge
            if caught[u] + weight < caught[v]:
                caught[v] = caught[u] + weight  # update the predecessor
                pred[v] = u
                allocs = allocations[u][:]
                for alloc in sorted(crossed_allocs, reverse=True):
                    del allocs[alloc]
                allocations[v] = allocs
                Q.add((v, caught[v]))

    target = max(list(targets.keys()), key=lambda t: targets[t]*(1-caught[t]))
    t = target
    while t is not None:
        attacker_response.insert(0, t)
        t = pred[t]
    return list(zip(attacker_response, attacker_response[1:])), target


def cost(edge, X, x):
    """
    This function calculates the weight of an edge which is the probability of the attacker to be caught associated
    to the edge.
    :param edge: extracted edge from the priority queue
    :param X: set of allocations of the defender
    :param x: mixed strategy of the defender
    :return: sum_cost: sum of the probabilities associated to the extracted edge. If the edge is in many allocations
    the probability to be caught going through this edge is higher.
    crossed_allocs: index of each defender allocation
    """
    sum_cost = 0

    crossed_allocs = []
    for index, allocation in enumerate(X):
        if edge in set(allocation):
            sum_cost += x[index]
            crossed_allocs.append(index)
    return sum_cost, crossed_allocs