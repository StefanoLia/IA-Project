import random as rd


def dbo(A, a, k, graph, target_values):
    """
    Defender Better Response (DBO) is a greedy approach to generate the better response Xg of the defender
    :param A: the set of paths of the attacker
    :param a: the mixed strategy of the attacker over a set of paths
    :param k: max number of the defender resources
    :param graph: set of the game
    :param target_value: value of the target to defend/attack
    :return: Xg the greedy defender allocation
    """

    defender_response = []  # better defender response Xg
    attacker_paths = A[:]  # Ar
    edge_list = list(graph.edges())
    weights = {}
    while len(defender_response) < k and attacker_paths:
        for (x, y) in edge_list:
            weights[(x, y)] = 0

        for index, aj in enumerate(attacker_paths):
            for e in aj:
                # weights[e] represents the total marginal usage of edge e in the attacker's mixed strategy a
                # weighted by the payoff of the target attacked by the attacker
                weights[e] = weights[e] + a[index] * target_values[index]

        max_edge = max(weights, key=weights.get)
        weights[max_edge] = -1
        edge_list.remove(max_edge) # sicuramente max_edge non viene più selezionato per l'allocazione
        defender_response.append(max_edge)

        to_remove = []
        for aj in attacker_paths:
            if max_edge in aj:
                to_remove.append(aj)
        print("attacchi intercettati con", max_edge, ":", to_remove)

        for path in to_remove:
           attacker_paths.remove(path)

    while len(defender_response) < k:
        rd.shuffle(edge_list)
        extracted_edge = edge_list.pop()
        defender_response.append(extracted_edge)

    return defender_response
