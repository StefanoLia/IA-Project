import numpy as np
from solver import *
from rugged import *
from snares import *
import matplotlib.pyplot as plt
from graph import Graph
import os


def to_path(attack):
    route = [e[0] for e in attack]
    print("attack!", attack)
    route.append((attack[-1])[1])
    return route


if __name__ == '__main__':

    range = np.linspace(0, 0.002, num=3)

    for filename in os.listdir('images/'):
        if filename.endswith('.png'):
            os.remove('images/'+filename)
    for filename in os.listdir('images/path_folder/'):
        if filename.endswith('.png'):
            os.remove('images/path_folder/'+filename)

    results = open("results.txt", 'w')

    G = Graph('Milan, Italy')
    G_nodes = G.get_real_nodes()
    print(G_nodes)
    real_source = rd.sample(G_nodes, 1)[0]
    ts = rd.sample(G_nodes, 3)
    real_targets = {}
    for t in ts:
        real_targets[t] = rd.random()*100

    targets = {}
    for target in real_targets:
        targets[G.get_cripted_node(target)] = real_targets[target]

    source = G.get_cripted_node(real_source)
    results.write("SOURCE: {} \n".format(real_source))
    for target in real_targets:
        results.write("TARGET: {} ".format(target))
        results.write("VALUE OF THE TARGET: {} \n".format(real_targets[target]))

    thr = 0.02
    num_allocations = 1

    results.write("NUMBER OF ALLOCATIONS USED: {} \n".format(num_allocations))
    results.write("THRESHOLD USED: {} \n".format(thr))

    for value in range:

        chi = value

        def_strategy, att_strategy = snares(G.get_graph(), num_allocations, source, targets, threshold=thr, chi=chi)
        attacks = [attack for index, attack in enumerate(att_strategy[0]) if ((att_strategy[1])[0])[index] > 0]
        routes = [to_path(attack) for attack in attacks]

        results.write("**************************** CHI: {} **************************** \n".format(chi))

        results.write("ATTACKS: {}\n".format(routes))
        results.write("ATTACKER MIXED STRATEGY: {}\n".format((att_strategy[1])[0]))
        results.write("ATTACKS UTILITY: {}\n".format((att_strategy[1])[1]))

        results.write("ALLOCATIONS: {}\n".format(def_strategy[0]))
        results.write("DEFENDER MIXED STRATEGY: {}\n".format((def_strategy[1])[0]))
        results.write("DEFENDER UTILITY: {}\n".format((def_strategy[1])[1]))

        x = np.arange(1, len(routes)+1)
        y = []
        media = 0
        for route in routes:
            y.append(len(route))
            media = media + len(route)
        media = float(media)/(len(routes))

        results.write("AVERAGE LEN PATH: {}\n".format(media))

        plt.clf()

        plt.plot(x, y)
        plt.title("Len routes")
        plt.xlabel("Path number")
        plt.ylabel("Number of edges")
        plt.savefig("images/len_routes{}.png".format(chi))


        G.save_graph_with_routes(routes, real_targets, chi)

        G.refactor()
