set Nodes;
set Edges within {i in Nodes, j in Nodes};
set Allocations;
set AllocationPaths{Allocations} within Edges;
set In {n in Nodes} := (setof {m in Nodes: (m, n) in Edges} (m, n));
set Out {n in Nodes} := (setof {m in Nodes: (n, m) in Edges} (n, m));

param X{i in Allocations, (m, n) in Edges} := if (m, n) in AllocationPaths[i] then 1 else 0;
param k > 0;
param x{Allocations};
param tvalue > 0;
param t;
param s;

var z {Allocations} >= 0, <= 1;
var gamma{Edges} binary;

maximize utility: tvalue*sum{i in Allocations} (1-z[i])*x[i];
s.t. source: sum{(m, n) in Out[s]} gamma[m, n] = 1;
s.t. sink: sum{(m, n) in In[t]} gamma[m, n] = 1;
s.t. path{n in Nodes : n != s && n != t}: (sum{(i, j) in In[n]: i != t} gamma[i, j]) = (sum{(i, j) in Out[n]: j != s} gamma[i, j]);
s.t. blocked{i in Allocations, (m, n) in Edges}: z[i] >= gamma[m, n] + X[i, m, n] - 1;