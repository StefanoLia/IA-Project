# Dichiarazione degli insiemi
set Attacks;
set Allocations;

# DICHIARAZIONE DEI PARAMETRI
param t {Attacks}; # TARGET
param z {Allocations, Attacks};

var a {j in Attacks} >= 0, <= 1;
var v;

maximize utility: v;
subject to constraint{i in Allocations}:
            sum {j in Attacks} (1 - z[i,j])*a[j]*t[j] >= v;
s.t. prob: sum{j in Attacks} a[j] = 1;
