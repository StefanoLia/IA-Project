# Dichiarazione degli insiemi
set Attacks;
set Allocations;

# DICHIARAZIONE DEI PARAMETRI
param t {Attacks}; # TARGET
param z {Allocations, Attacks};

var x {i in Allocations} >= 0, <= 1;
var v;

maximize utility: v;
subject to constraint{j in Attacks}:
            -t[j]*sum {i in Allocations} (1 - z[i,j])*x[i] >= v;
s.t. prob: sum{i in Allocations} x[i] = 1;