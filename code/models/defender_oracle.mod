set Nodes;
set Edges within {i in Nodes, j in Nodes};
set Attacks;
set AttackPaths{Attacks} within Edges;

param A{j in Attacks, (m, n) in Edges} := if (m, n) in AttackPaths[j] then 1 else 0;
param k > 0;
param a{Attacks};
param t{Attacks};

var z {Attacks} >= 0, <= 1;
var lambda{Edges} binary;

maximize utility: -sum{j in Attacks} (1-z[j])*a[j]*t[j];
s.t. blocked{j in Attacks}: z[j] <= sum{ (m, n) in Edges} A[j, m, n]*lambda[m, n];
s.t. numAllocations: sum { (m, n) in Edges} lambda[m, n] <= k;
