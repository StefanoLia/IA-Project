#P parametri che varia nell'insieme dei prodotti
#gli insiemi stanno nel file .dat

#DICHIARAZIONE INSIEMI
set Prodotti;
set Risorse;

#DICHIARAZIONE PARAMETRI
param maxNumProd {Prodotti};
param P {Prodotti}; 
param Q {Risorse};
param A {Prodotti, Risorse};

var x {i in Prodotti} >=0, <= maxNumProd[i] integer;

maximize profitto: sum{i in Prodotti} P[i]*x[i];
subject to disponib{j in Risorse}:
			sum {i in Prodotti} A[i,j]*x[i] <= Q[j];	