import random as rd
import networkx as nx
from coreLP import coreLP
from utils.oracles import DO, DA


def rugged(graph, resources, source, target, target_value):
    """

    :param graph: where the game is set
    :param resources: defender allocations
    :param source: starting point(s)
    :param target: target point(s)
    :param target_value: the value associated to the target
    :return: (x, a) the minmax strategy for the attacker a and minmax allocation edges x for the defender
    """
    allocations = []  # allocations of the defender
    attacks = []  # attacks of the attacker
    edges = list(graph.edges())
    nodes = list(graph.nodes())

    print("edges: ", edges)
    print("nodes: ", nodes)

    # Initialize allocations using an arbitrary candidate defender allocation

    # random first allocation
    edges_2 = list(graph.edges())
    rd.shuffle(edges_2)
    edges_added = 0
    allocation = []
    while edges_added < resources and edges_2:
        allocation.append(edges_2.pop())
        edges_added += 1
    print("1st random allocation: ", allocation)
    allocations.append(allocation)

    # Initialize attack using an arbitrary candidate attacker path

    # random first attack path
    path = list(nx.all_simple_paths(graph, source=source, target=target)).pop()
    attack = []
    for i in range(len(path)-1):
        if path[i] > path[i+1]:
            attack.append((path[i+1], path[i]))
        else:
            attack.append((path[i], path[i+1]))

    print("1st random attack: ", attack)
    attacks.append(attack)

    z = [[0]]
    if not set(attack).isdisjoint(allocation):  # if defender blocks the attacker
        z[0][0] = 1  # allocation 0 blocks the attacks 0
    print("z", z)

    convergence = False

    while not convergence:

        print("Attacks:", attacks)
        print("Allocations:", allocations)

        # coreLP: returns the mixed strategy of the attacker and defender
        mixed_defender, mixed_attacker = coreLP(attacks, allocations, [target_value for i in range(len(attacks))], z)
        x = mixed_defender[0]
        a = mixed_attacker[0]

        defender_utility = mixed_defender[1]
        attacker_utility = mixed_attacker[1]

        # ### Chiamata a Defenser Oracle ###
        defender_allocation, defender_z, defence_utility = DO(nodes, edges, attacks, resources, target_value, a)

        # ### Chiamata ad Attacker Oracle ###
        attacker_path, attacker_z, attack_utility = DA(nodes, edges, allocations, resources, target, target_value,
                                                       source, x)

        # Adding the new allocations and the new attacker path
        allocations.append(defender_allocation)
        attacks.append(attacker_path)

        # Updating of the matrix z before by columns (attacks) and than by rows (allocations)
        for i, val in enumerate(z):
            val.append(attacker_z[i])

        z.append(defender_z)

        # inserting the missing value of z
        z[len(allocations)-1].append(0)
        if not set(defender_allocation).isdisjoint(attacker_path):
            z[len(allocations)-1][len(attacks)-1] = 1

        print("z: ", z)

        print(defender_allocation)

        print("TEST:", defence_utility, mixed_defender[1])

        if defence_utility <= defender_utility and \
                attack_utility >= attacker_utility:
            convergence = True
            print("******** Convergenza Rugged Raggiunta ********")
        else:
            print("******** ITERAZIONE ********")

    return allocations[-1], attacks[-1]









