import networkx as nx
import random as rd
from coreLP import coreLP
from heuristics.dbo import dbo
from heuristics.abo import abo
from heuristics.abo_reduced import abo_reduced
from utils.oracles import *


def get_utility_defender(z, target_values, a):
    utility = 0
    for index, prob in enumerate(a):
        utility = utility - (1 - z[index]) * prob * target_values[index]
    return utility


def get_utility_attacker(z, target_value, x):
    utility = 0
    for index, prob in enumerate(x):
        utility = utility + (1 - z[index]) * prob * target_value
    return utility


def get_min_cut(graph, s, t):
    cut_value, partition = nx.minimum_cut(graph, s, t, capacity='weight')
    reachable, non_reachable = partition
    cutset = set()
    for u, nbrs in ((n, graph) for n in reachable):
        cutset.update((u, v) for v in nbrs if v in non_reachable and (u, v) in graph.edges)

    cut_set = sorted(cutset)
    # print("MIN_CUT_EDGES", cut_set)
    return cut_set


def min_cut_fanout(graph, k, allocations, attacks, source, targets, n_iter):
    """
    the objective of warm-starts is to generate such strategies for both players and add them before the start of the
    double-oracle iterative procedure. It will sample pure strategy for the defender
    :param graph:
    :param k: the number of strategies to insert (it is supposed to be < resources)
    :param allocations: the defender strategy to fill in
    :param attacks: the attacker strategy to fill in
    :param source: the node where the attacker starts his path
    :param targets: the nodes where the attacker wants to arrive
    :param n_iter: max number of iterations
    :return: k defender pure strategy (allocations) and z attacker pure strategy (z <= k)
    """
    edges = list(graph.edges())
    INFINITY = float('inf')

    print(graph.nodes())
    print("EDGE", graph.edges())
    targetlist = sorted(list(targets.keys()), key=lambda x:targets[x])
    target = targetlist.pop(0)
    for n in range(n_iter):
        allocation = []
        edges_of_min_cut = get_min_cut(graph, source, target)
        file = open("log.txt", 'w')
        file.write("EDGES IN MINCUT: {}\n".format(len(edges_of_min_cut)))
        i = 0
        while i < k and edges_of_min_cut != []:
            rd.Random(80).shuffle(edges_of_min_cut)
            # print("edges", edges)
            extracted_edge = edges_of_min_cut.pop()

            if extracted_edge not in allocation:
                allocation.append(extracted_edge)
            # print("edge estratto: ", extracted_edge)
            # print("campo peso: ", graph[extracted_edge[0]][extracted_edge[1]])
            graph[extracted_edge[0]][extracted_edge[1]]['weight'] = 998
            i = i + 1

        path = nx.dijkstra_path(graph, source, target)
        # print("path", path)

        allocations.append(allocation)
        # print("allocations", allocations)

        tuple_path = list(zip(path, path[1:]))
        for edge in tuple_path:
            graph[edge[0]][edge[1]]['weight'] = 998

        if tuple_path not in attacks:
            attacks.append(tuple_path)
        elif targetlist:
            target = targetlist.pop(0)
        # print("attacks", attacks)

    return allocations, attacks


def snares(graph, resources, source, targets, threshold=0.001, chi=0):
    """
    This function calculates a mixed strategy of the attacker and of the defender trying to improve the performance
    of rugged using some heuristics: dbo for the defender and abo for the attacker
    :param graph: it represents the urban area
    :param resources: number of resources of the defender
    :param source: the start point of the attacker
    :param targets: the targets to defend for the defender and to attack for the attacker
    :param threshold: point of stopping
    :param chi: penalization term
    :return: defender allocations and mixed strategy and attacker paths and mixed strategy
    """
    allocations = []  # allocations of the defender
    attacks = []  # attacker attack paths

    edges = list(graph.edges())
    nodes = list(graph.nodes())

    file = open("log.txt", 'a+')
    # file.write("GRAPH NODES")
    # file.writelines(str(index)+" "+str(n)+"\n" for index,n in enumerate(nodes))
    best_target = max(targets, key=lambda x:targets[x])
    if chi == 0:
        allocations, attacks = min_cut_fanout(graph, resources, allocations, attacks, source, {best_target:targets[best_target]}, 4)
    else:
        allocations, attacks = min_cut_fanout(graph, resources, allocations, attacks, source, targets, 4)

    target_values = [targets[target] for target in [attack[-1][1] for attack in attacks]]
    z = []
    for allocation in allocations:
        row = []
        for attack in attacks:
            if not set(attack).isdisjoint(allocation):  # if defender blocks the attacker
                row.append(1)  # allocation 0 blocks the attacks 0
            else:
                row.append(0)
        z.append(row)
    file.write("\nnumero attacchi min_cut: {}".format(len(attacks)))
    file.write("\nnumero difese min_cut: {}".format(len(allocations)))
    file.write("\nATTACKS min_cut: {}".format(attacks))
    file.write("\nALLOCATIONS min_cut {}".format(allocations))
    file.write("\nz matrix {}".format(z))

    mixed_attack_utility = 0
    utility_att = float('inf')

    mixed_defence_utility = 0
    utility_def = float('inf')

    while utility_att - mixed_attack_utility > threshold or \
            utility_def - mixed_defence_utility > threshold:
        file.write("\n*****************************INIZIO CICLO*****************************")
        # ### CORE LP ###
        print("z:", z)
        print("attacks", len(attacks))
        print("allocations", len(allocations))
        mixed_defender, mixed_attacker = coreLP(attacks, allocations, target_values, z)

        mixed_attack_utility = mixed_attacker[1]
        mixed_defence_utility = mixed_defender[1]

        file.write("\nCORELP: Mixed attack utility: {}".format(mixed_attack_utility))
        file.write("\nCORELP: Mixed attack strategy: {}".format(mixed_attacker[0]))
        file.write("\nCORELP: Mixed defence utility: {}".format(mixed_defence_utility))
        file.write("\nCORELP: Mixed defence strategy: {}".format(mixed_defender[0]))
        # ### Better defender response ###
        X = dbo(attacks, mixed_attacker[0], resources, graph, target_values)

        row_def = []
        for attack in attacks:
            if not set(attack).isdisjoint(X):  # if defender blocks the attacker
                row_def.append(1)  # allocation 0 blocks the attacks 0
            else:
                row_def.append(0)

        utility_def = get_utility_defender(row_def, target_values, mixed_attacker[0])

        file.write("\nDEFENDER EURISTIC: Euristic allocation: {}".format(X))
        file.write("\nDEFENDER EURISTIC: Euristic utility: {}".format(utility_def))

        if utility_def - mixed_defence_utility <= 0.001:
            X, row_def, utility_def = DO(nodes, edges, attacks, resources,
                                         target_values, mixed_attacker[0])
            file.write("\nDEFENDER ORACLE: Oracle allocation: {}".format(X))
            file.write("\nDEFENDER ORACLE: Oracle utility: {}".format(utility_def))

        A, t = abo_reduced(allocations, mixed_defender[0], graph, source, targets, chi=chi)
        row_att = []

        for allocation in allocations:
            if not set(A).isdisjoint(allocation):  # if defender blocks the attacker
                row_att.append(1)  # allocation 0 blocks the attacks 0
            else:
                row_att.append(0)
        print("\nEURISTIC TARGET: ", t)
        utility_att = get_utility_attacker(row_att, targets[t], mixed_defender[0])

        file.write("\nATTACKER EURISTIC: Euristic attack: {}".format(A))
        file.write("\nATTACKER EURISTIC: Euristic target: {}".format(t))
        file.write("\nATTACKER EURISTIC: Euristic utility: {}".format(utility_att))

        if utility_att - mixed_attack_utility <= 0.001:
            A = []
            row_att = []
            utility_att = float('-inf')
            t = None
            for target in list(targets.keys()):
                attack, row, utility = DA(nodes, edges, allocations, resources, target, targets[target], source, mixed_defender[0])
                if utility > utility_att:
                    A = attack
                    row_att = row
                    utility_att = utility
                    t = target
            file.write("\nATTACKER ORACLE: Oracle allocation: {}".format(A))
            file.write("\nATTACKER ORACLE: Oracle target: {}".format(t))
            file.write("\nATTACKER ORACLE: Oracle utility: {}".format(utility_att))

        attacks.append(A)
        target_values.append(targets[t])
        allocations.append(X)

        for index, row in enumerate(z):
            row.append(row_att[index])

        found = 0
        for a in A:
            if a in X:
                found = 1

        row_def.append(found)
        z.append(row_def)

        file.write("\nz matrix {}".format(z))
        file.write("\nattacker utility diff: {}".format(abs(utility_att - mixed_attack_utility)))
        file.write("\ndefender utility diff: {}".format(abs(utility_def - mixed_defence_utility)))
        file.write("\n******************************FINE CICLO******************************")

    mixed_defender, mixed_attacker = coreLP(attacks, allocations, target_values, z)
    file.write("\nCORELP: Mixed attack utility: {}".format(mixed_attacker[1]))
    file.write("\nCORELP: Mixed attack strategy: {}".format(mixed_attacker[0]))
    file.write("\nCORELP: Mixed defence utility: {}".format(mixed_defender[1]))
    file.write("\nCORELP: Mixed defence strategy: {}".format(mixed_defender[0]))

    return (allocations, mixed_defender), (attacks, mixed_attacker)
