from amplpy import *


class Solver:
    def __init__(self, model, sets_values, parameters_values, indexed_sets_values=[]):
        self.ampl = AMPL()

        self.ampl.read(model)
        self.model = model.replace("./models/", "")

        self.objectives = self.ampl.getObjectives()
        self.parameters = self.ampl.getParameters()
        self.sets = self.ampl.getSets()

        self.result = []
        self.set_sets(sets_values)
        self.set_indexed_sets(indexed_sets_values)
        self.set_parameters(parameters_values)

    def solve(self):
        print('******** resolving {} ********'.format(self.model))
        self.ampl.solve()
        for _objective in self.objectives:
            print('Result of '+str(_objective[0])+': {},'.format(_objective[1].value()))
            self.result.append(_objective[1].value())

    def set_sets(self, sets_values):
        # imposto i valori dei set
        for _set in sets_values:
            # print(self.sets[_set], sets_values[_set])
            self.sets[_set].setValues(sets_values[_set])

    def set_indexed_sets(self, indexed_sets_values):
        for _indexed in indexed_sets_values:
            num_instances = len(indexed_sets_values[_indexed])
            # print("num_istances", self.sets[_indexed])
            for i in range(num_instances):
                _instance = self.sets[_indexed].get(i)
                _instance.setValues(indexed_sets_values[_indexed][i])

    def set_parameters(self, parameters_values):
        for _parameter in parameters_values:
            # print(self.parameters[_parameter], parameters_values[_parameter])
            if parameters_values[_parameter][0]:  # if the parameter is scalar
                self.parameters[_parameter].set(parameters_values[_parameter][1])
            else:
                self.parameters[_parameter].setValues(parameters_values[_parameter][1])

    def get_parameters(self):
        for param in self.parameters:
            print(param[1])
        return self.parameters

    def print_variables_values(self):
        variables = self.ampl.getVariables()
        for var in variables:
            print(var[1].getValues())

    def get_sets(self):
        for _set in self.sets:
            print(_set[1])
        return self.sets

    def get_variable(self, variable):
        return self.ampl.getVariable(variable)

    def get_objective(self):
        return self.result[0]

    def reset(self):
        self.ampl.reset()
