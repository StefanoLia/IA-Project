from itertools import count
from heapq import heappop, heappush


class MinHeap:
    def __init__(self):
        self.pq = []  # list of entries arranged in a heap
        self.entry_finder = {}  # mapping of tasks to entries
        self.REMOVED = '<removed-task>'  # placeholder for a removed task
        self.removed_count = 0
        self.counter = count()  # unique sequence count

    @property
    def empty(self):
        return len(self.pq) - self.removed_count == 0

    def add(self, taskp):
        'Add a new task or update the priority of an existing task'
        task = taskp[0]
        priority = taskp[1]
        if task in self.entry_finder:
            self.remove_task(task)
            self.removed_count += 1
        count = next(self.counter)
        entry = [priority, count, task]
        self.entry_finder[task] = entry
        heappush(self.pq, entry)

    def remove_task(self, task):
        'Mark an existing task as REMOVED.  Raise KeyError if not found.'
        entry = self.entry_finder.pop(task)
        entry[-1] = self.REMOVED

    def pop(self):
        'Remove and return the lowest priority task. Raise KeyError if empty.'
        while self.pq:
            priority, count, task = heappop(self.pq)
            if task is not self.REMOVED:
                del self.entry_finder[task]
                return task
            else:
                self.removed_count -= 1
        raise KeyError('pop from an empty priority queue')

    def __contains__(self, item):
        return item in list(self.entry_finder.keys())
