from solver import Solver


def rearrange(edges, source, sink):
    print("Original attack: ", edges)
    node = source
    n = len(edges)
    arranged = []
    while node != sink:
        edge = [e for e in edges if e[0] == node][0]
        # print("edge: ", edge)
        edges.remove(edge)
        arranged.append(edge)
        node = edge[1]
    return arranged


def DO(nodes, edges, attacks, resources, target_values, a):
    # defender oracle: returns the best pure strategy of the defender given a mixed strategy of the attacker
    sets = {"Nodes": nodes, "Edges": edges, "Attacks": [i for i in range(len(attacks))]}
    indexed_sets = {"AttackPaths": attacks}
    parameters = {'k': (True, resources), 't': (False, target_values), 'a': (False, a)}

    defender_oracle_mod = "./models/defender_oracle.mod"
    defender_oracle_solver = Solver(defender_oracle_mod, sets, parameters, indexed_sets)

    defender_oracle_solver.solve()

    # print("Defender variables: ")
    # defender_oracle_solver.print_variables_values()

    defender_allocation = [(int(tuple[0]), int(tuple[1])) for tuple in defender_oracle_solver.get_variable('lambda').getValues().toList() if tuple[2] == 1.0]
    defender_z = [int(tuple[1]) for tuple in defender_oracle_solver.get_variable('z').getValues().toList()]

    print("Defender allocations:", defender_allocation)

    return defender_allocation, defender_z, defender_oracle_solver.get_objective()


def DA(nodes, edges, allocations, resources, target, target_value, source, x):
    sets = {"Nodes": nodes, "Edges": edges, "Allocations": [i for i in range(len(allocations))]}
    indexed_sets = {"AllocationPaths": allocations}
    parameters = {'k': (True, resources), 't': (True, target), 'tvalue':(True, target_value), 's':(True, source), 'x': (False, x), }

    # attacker_oracle: returns the best pure strategy of the attacker given a mixed strategy of the defender
    attacker_oracle_mod = "./models/attacker_oracle.mod"
    attacker_oracle_solver = Solver(attacker_oracle_mod, sets, parameters, indexed_sets)

    attacker_oracle_solver.solve()

    # print("Attacker variables: ")
    # attacker_oracle_solver.print_variables_values()

    attacker_path = rearrange([(int(tuple[0]), int(tuple[1])) for tuple in attacker_oracle_solver.get_variable('gamma').getValues().toList() if tuple[2] == 1.0], source, target)
    attacker_z = [int(tuple[1]) for tuple in attacker_oracle_solver.get_variable('z').getValues().toList()]

    return attacker_path, attacker_z, attacker_oracle_solver.get_objective()