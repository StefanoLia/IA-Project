\section{Application of Game Theory principles}
To fully understand the implications entailed by choosing Game Theory as the reference theoretical framework for the problem, we decided to study the most important theorems referenced by the papers and the core ideas behind Game Theory strategy games.\\
In Game Theory we model a multi-agent scenario where each agent can choose among different actions as a \textit{game} \cite{strategic-games}, that is a pair:\\
\begin{center}
$ G = (S, u)$
\end{center}
where
\begin{itemize}
	\item $S$ is a \textit{profile of strategy sets}, namely a list $S = [S_{1},\ ...\ ,S_{N}]$ indexed on the N players involved in the game which associates to each player the set of all the possible actions he/she can perform;
	\item $u$ is a \textit{profile of utility functions}, that is a list $u = [u_{1},\ ...\ ,u_{N}]$ of N functions, one for each player, where each function has the form: $u_{i}: S_{1}\times\ ...\ \times S_{N} \rightarrow \mathbb{R}$. 
\end{itemize}
To play such a game, each player $i$ chooses a strategy $s_{i} \in S_{i}$. This yields the \textit{strategy profile} $s = [s_{1},\ ...\ ,s_{N}]$, or \textit{outcome} of the game. The utility of the outcome for player $j$ is the value $u_{j}(s)$.\\
\textit{Zero-sum} games are used to model situations of fierce competition and involves two players, say $i \in \{1,2\}$, with $u_{1}(s) = -u_{2}(s)$ for all strategy profiles $s$.\\
The network security scenario is modeled as a \textit{zero-sum} game with only two players: the attacker and the defender, where the sets of pure strategies and the utility functions for each player are the same as described at the end of the previous section.\\
The aim of the algorithms described in the papers is to find the \textit{best} strategy for the attacker, that is the best allocation of resources against all the possible attack paths the attacker can choose from.\\
We define the \textit{best} defense strategy as a \textit{punishment strategy} which minimizes the maximum utility the attacker can gain - a \textit{min-max} strategy.\\
At the same time we want the defender to act in an unpredictable way and we want him to choose the allocation strategy according to a probability distribution over the set of all possible allocation of resources on the network graph.\\
We can define the \textit{mixed extension} of a game as follows.\\
Let $G = (S, u)$ be a finite game. For every player $i$, $\hat{S}_{i}$ is the set of probability distributions over $S_{i}$, that is the set of functions $\sigma_{i}: S_{i} \rightarrow [0,1]$ with:
\begin{center}
$\sum \limits_{s_{i} \in S_{i}} \sigma(s_{i}) = 1$
\end{center}
Furthermore, if we consider a two player game, the expected utility when playing according to $\sigma_{1}$ for player $1$ is:
\begin{center}
$\hat{u}_{1}(\sigma_{1}, \sigma_{2}) := \sum \limits_{a_{i} \in S_{1}} \sum \limits_{b_{j} \in S_{2}} \sigma_{1}(a_{i}) \times \sigma_{2}(b_{j}) \times u_{1}(a_{i}, b_{j})$
\end{center}
It is also possible to calculate the expected utility for player $1$ when playing a pure strategy $a$ against a mixed strategy $\sigma_{2}$ selected by the opponent:
\begin{center}
$u_{1}(a, \sigma_{2}) = \sum \limits_{b_{j} \in S_{2}} \sigma_{2}(b_{j}) \times u_{1}(a, b_{j})$
\end{center}
The mixed extension of $G$ for a set $N$ of players is the game $\hat{G} = (\hat{S}, \hat{u})$. Thus, when playing a mixed extension of a game, a player first selects one of the possible probability distributions over his strategy set and then chooses to play a pure strategy according to such distribution.\\
The mixed extension of a zero-sum two-player game has many interesting and useful properties.\\
First of all, for the defender (and conversely for the attacker) a min-max mixed strategy, that is a strategy which minimizes the maximum \textit{expected} utility of the attacker, is the same as a max-min mixed strategy, that is a strategy which maximizes the minimum \textit{expected} utility the defender can gain (\textbf{von Neumann Theorem}).\\
In general, we define a \textit{Nash Equilibrium} as a strategy profile in which each strategy is optimal for its player. In the scenario of a mixed extension of a two-player zero-sum game, a Nash Equilibrium is a mixed strategy profile $(\sigma_{1}, \sigma_{2})$ such that: 
\begin{center}
$\forall\ \sigma_{i} \in \hat{S_{1}},\ \hat{u}_{1}(\sigma_{1}, \sigma_{2}) >= \hat{u}_{1}(\sigma_{i}, \sigma_{2})$\\
and\\
$\forall\ \sigma_{j} \in \hat{S_{2}},\ \hat{u}_{2}(\sigma_{1}, \sigma_{2}) >= \hat{u}_{2}(\sigma_{1}, \sigma_{j})$
\end{center}
In particular, in a Nash Equilibrium for a mixed two-player zero-sum game each strategy, in addition to being optimal, is also a min-max and max-min strategy for each player.\\
Another relevant result is the following.\\ 
Given a mixed strategy $\sigma_{i}$, we call \textit{support} of $\sigma_{i}$ the set of all strategies $ s \in S_{i}$ such that $\sigma_{i}(s) > 0$. A mixed strategy pair $(\sigma_{1}, \sigma_{2})$ is a Nash Equilibrium if and only if for all pure strategies $a$ in the support of $\sigma_{i}$, $a$ is a best response to $\sigma_{-i}$. As a consequence, if the mixed strategy pair $(\sigma_{1}, \sigma_{2})$ is a Nash Equilibrium and $a$ and $b$ are both in the support of $\sigma_{1}$, then $u_{1}(a, \sigma_{2}) = u_{1}(b, \sigma_{2})$, and thus player $1$ is indifferent between $a$ and $b$.\\
So, if we model the network security problem as a two-player zero-sum game and consider its mixed extension, if we compute a Nash Equilibrium we can obtain the desired min-max mixed strategy for the defender. Besides, each allocation of resources in the support of such a strategy allows the defender to gain the same expected utility against the attacker's best mixed strategy.

