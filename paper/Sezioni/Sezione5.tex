\section{Experimental results}

In this section, we describe the experimental results we achieved thanks to our improvement to the original design of SNARES. We compare the performance of the 'classic' version of SNARES with that of our own 'modified' version which includes the penalization term $\chi$ and the new min-cut algorithm.

\subsection{Technological stack}
To run both the algorithms we had to implement the procedures described in the papers and their submodules.\\
The code of the project - completely accessible via GitHub - is structured in the following way:
\begin{itemize}
\item the \texttt{main} method, the bulk of \texttt{snares} and \texttt{rugged} and the heuristics employed by the former - \texttt{min\textunderscore cut\textunderscore fanout}, \texttt{abo} (\textit{Better Attacker Response}) and \texttt{dbo} (\textit{Better Defender Response}) - are written in \texttt{Python 3.6.4};
\item the integer and mixed integer linear programs used to compute the mixed Nash Equilibrium for both the attacker and the defender (\texttt{coreLP}) and the oracles (\texttt{DO} - \textit{Defender Oracle} -, and \texttt{AO} - \textit{Attacker Oracle}) are written following the \texttt{AMPL} syntax for \texttt{.mod} files. Necessary data is feed to the \texttt{.mod} files using the proper methods provided by the \texttt{amplpy} library;
\item The default solver used by our \texttt{AMPL} installation (v. 20180511) is \texttt{CPLEX} (v. 12.8.0.0).
\end{itemize}
To make the code more readable, understandable and structured we split all the \texttt{.py} and \texttt{.mod} files among different folders - the packages of the project.\\
Furthermore, we coded additional classes in order to encapsulate utility operations and make the \texttt{Python} code interact with \texttt{AMPL} files through standard 'interfaces'.\\ 
Among such classes the most important are:
\begin{itemize}
	\item \texttt{solver}, which allows us to feed necessary data directly into the \texttt{.mod} files;
	\item \texttt{graph}, which allows us to retrieve urban directed graphs from the OpenStreetMap platform, pass them as inputs to our methods and show the results of the computation in a clear, graphical way.
\end{itemize}
\begin{center}
	\begin{figure}
		\includegraphics[scale=0.60]{images/technological_stack.png}
		\caption{Graphical representation of the dependencies between the core modules of the project}
	\end{figure}
\end{center}

\subsection{Graph of the urban area}

To plot the urban network images shown in this section we used library \textit{osmnx} provided by \cite{osmnx}. To make the maps and the results more readable, we extended \textit{osmnx} by including the following functionalities:
\begin{itemize}
\item show the different levels of importance of the targets nodes: the size of each target is proportional to the level of importance set by the user before running the main function or randomly by the program itself;
\item distinguish between source nodes and target nodes: we decided to color the source node in blue and the target nodes in red;
\item plot more than one path on the map and use different colors in case of multiple attacks.
\end{itemize} 
The new version of the library and the instruction to install it are available \href{https://github.com/StefanoLia/osmnx}{here} \cite{osmnx-modified}.\\
Thanks to this external module we can show a map where we plot all the paths in the support of the mixed strategy for the attacker. Then, he/she selects one of such attacks to play against the defender according to the probability distribution defined over them.
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.45]{images/example_image.png}
	\caption{Example of a typical result of the program}
	\label{image-result}
\end{figure}

\begin{figure}[t]
	\includegraphics[scale=0.45]{images/result_file.png}
	\caption{A simple example of the result file produced by the program}
	\label{result}
\end{figure}

\begin{figure}[h!]
	\includegraphics[scale=0.45]{images/Milan_Italy_graph_with_routes_00.png}
	\caption{The mixed strategy for the attacker provided by the "original" SNARES}
	\label{snares-result-map}
\end{figure}

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.45]{images/Milan_Italy_graph_with_routes_0001.png}
	\caption{The mixed strategy for the attacker provided by the "modified" SNARES}
	\label{modified-snares-result-map}
\end{figure}

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.45]{images/Milan_len_routes00.png}
	\caption{Length of the paths found by SNARES in Milan}
	\label{snares-result-length}
\end{figure}

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.40]{images/rome_more_routes.png}
	\caption{Attack paths in Rome using the original version of SNARES}
	\label{rome-more-routes}
\end{figure}

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.36]{images/Milan_len_routes0001.png}
	\caption{Length of the paths found by the "modified" SNARES in Milan}
	\label{modified-snares-result-length}
\end{figure}

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.38]{images/rome_fewer_routes.png}
	\caption{Attack paths in Rome using the modified version of SNARES}
	\label{rome-fewer-routes}
\end{figure}

\subsection{Output File Results}
As a result of running our code we provide a file, \texttt{results.txt}, which shows the different mixed and pure strategies calculated for the attacker and for the defender.\\
These strategies are computed using different values of the \textit{penalization term} $\chi$ which allows to compare the results associated with each level of $\chi$.\\ 
Moreover, this file reports the node chosen as the source, the nodes chosen as the targets and their associated level of importance.\\ 
Figure \ref{result} provides an example of such file which has the following structure:
\begin{itemize}
	\item SOURCE: the starting point chosen by the attacker;
	\item LIST of TARGETS: all the target nodes with their associated value of importance;
	\item ALLOCATIONS: the number of allocations played by the defender;
	\item THRESHOLD: the cut-off used as a stopping criterion for SNARES;
	\item The results obtained using different values of $\chi$. For each value of $\chi$ we list:
	\begin{itemize}
		\item pure and mixed strategies for both players;
		\item the utility corresponding to each mixed strategy;
		\item the average length of the attack paths.
	\end{itemize}
	 Notice that if $\chi = 0$ the penalization term is not used.
\end{itemize}

\subsection{Comparison with empirical evidences}

Figure \ref{image-result} presents a typical output map in which all the paths in the support of the attacker's mixed strategy are drawn. This plot highlights the differences between the solutions provided by the original version of SNARES and the "modified" version described in the previous section. \\ \\
As shown in figure \ref{result} and in figure \ref{snares-result-map}, the original SNARES algorithm, which corresponds to the modified version with $\chi = 0$, finds 2 attack paths each of which selected by the attacker with probability equal to 0.5. The defender allocates only 2 resources over the edges of the graph and the resulting expected \textit{Defender Utility} is 16.05 (which is the opposite of the resulting expected \textit{Attacker Utility}: -16.05). To better understand the advantages of the inclusion of the penalization term we also provide the average length of the attack  paths.\\ \\
\textbf{Average length of the paths:} 23.0 ($\chi$ = 0) \\ \\
The modified version provides slightly different results (also reported in figure \ref{result} and graphically in figure \ref{modified-snares-result-map}). Using the \textbf{penalization term} $\chi$ = 0.001 one of the paths found by the attacker has fewer edges providing a better overall solution. As stated in the previous section, shorter paths are better than longer ones because the attacker is less likely to get caught by the defender if he/she crosses fewer edges.\\
Besides, the expected defender utility with $\chi = 0$ is the same as the expected defender utility with $\chi\ =\ 0.001$.\\
Following on from these results, it's clear that the average path length has decreased. As a matter of fact, it corresponds to: \\ \\
\textbf{Average length of the paths} = 18.0. ($\chi$ = 0.001) \\ \\
From this perspective, the attacker should prefer the second strategy to reach the target earlier. Therefore, empirical evidence suggests that a \textbf{penalization term} can be useful to improve the attacker's strategies.\\
As an additional proof of evidence, you can tell the difference between the two solutions in terms of the length of each attack path by comparing figure \ref{snares-result-length} and figure \ref{modified-snares-result-length}.\\
Also, the improvements in the min-cut submodule resulted in better performances with respect to the original procedure.\\
In fact, better initial strategies for both the attacker and the defender allows to decrease the number of iterations needed to reach convergence and the number of invocations to the oracles NP-Hard modules.\\
Figures \ref{rome-more-routes} and \ref{rome-fewer-routes} show a stark difference between the results provided by both the procedures.\\ Fewer attacks paths mean fewer iterations of the main loop in SNARES, and therefore a more efficient and time-consuming run of the algorithm.